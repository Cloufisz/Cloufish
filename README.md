### Hi there, I'm Bartholomew - aka [Cloufish][website] 👋

[![Website](https://img.shields.io/badge/WEBSITE-UP-blue)](https://cloufish.github.io/)
[![Blog](https://img.shields.io/badge/BLOG-UP-blue)](https://cloufish.github.io/blog)




## I'm ITSec enthusiast and love to automate my daily tasks (and those IT related too ;) )

- 🔭 I’m currently working on: Doing TryHackMe rooms, Portswigger Academy and Bug Bounty Hunting
- 🌱 I’m currently learning: Parallelism in Bash, awk, sed, Ansible, Kubernetes, Code Testing Frameworks like Jest, Jenkins and other CI/CD tools
- 👯 I’m looking to collaborate on: Dev**Sec**Ops projects
- 🤔 I’m looking for help with [Awesome-Worldwide-ITSecurity-Specialists](https://github.com/Cloufish/Awesome-Worldwide-ITSecurity-Specialists) - If you're non-native english speaker then please help! It'll only take around 20minutes! Thanks a lot! 
- ⚡ Fun fact: I love playing guitar, singing and also learn Estonian language and Japanese!

### Connect with me:

[<img align="left" alt="cloufish.github.io" width="22px" src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" />][website]
[<img align="left" alt="Cloufish | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
Discord: Cloufish#2977
<br />

### Languages and Tools:


<img align="left" alt="Visual Studio Code" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
<img align="left" alt="Git" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/git/git.png" />
<img align="left" alt="GitHub" width="26px" src="https://raw.githubusercontent.com/github/explore/78df643247d429f6cc873026c0622819ad797942/topics/github/github.png" />
<img align="left" alt="Terminal" width="26px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/terminal/terminal.png" />

<br />
<br />

---

### 📕 Latest Blog Posts

<!-- BLOG-POST-LIST:START -->
- [blackarch-zsh - How I created blackarch-zsh AND How you can create your own too :)](https://cloufish.github.io/blog/posts/creating-your-own-docker-pentesting-container/)
- [TryHackMe - Pickle Rick - WRITE-UP](https://cloufish.github.io/blog/posts/Pickle_Rick-en/)
- [blackarch-zsh - Ideal Docker container for hacking!](https://cloufish.github.io/blog/posts/blackarch-zsh-en/)
- [TryHackMe - Basic Pentesting WRITE-UP](https://cloufish.github.io/blog/posts/basic_pentesting/)
- [Is this blog dead?](https://cloufish.github.io/blog/posts/is-this-blog-dead-en/)
<!-- BLOG-POST-LIST:END -->

➡️ [more blog posts...](https://cloufish.github.io/blog/)

---

<details>
  <summary>:zap: GitHub Stats</summary>

  [![Cloufish's github stats](https://github-readme-stats.vercel.app/api?username=Cloufish&theme=algolia&show_icons=true)](https://github.com/anuraghazra/github-readme-stats)

</details>

[website]: https://cloufish.github.io/
[blog]: https://cloufish.github.io/blog/
[linkedin]: https://linkedin.com/in/cloufish
